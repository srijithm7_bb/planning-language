
//https://dzone.com/articles/how-to-add-jars-to-a-jetbrains-mps-project
import java.awt.BorderLayout;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import com.mindfusion.common.DateTime;
import com.mindfusion.common.DayOfWeek;
import com.mindfusion.common.Duration;
import com.mindfusion.drawing.Colors;
import com.mindfusion.drawing.GradientBrush;
import com.mindfusion.scheduling.Calendar;
import com.mindfusion.scheduling.CalendarView;
import com.mindfusion.scheduling.ThemeType;
import com.mindfusion.scheduling.model.Appointment;
import com.mindfusion.scheduling.model.Recurrence;
import com.mindfusion.scheduling.model.Schedule;
import com.mindfusion.scheduling.model.Style;

public class Planner extends JFrame
{
	static Calendar calendar = new Calendar();
    public static void main(String[] args)
    { 	
        SwingUtilities.invokeLater(new Runnable() {
             public void run() {
                 try {
                     new Planner().setVisible(true);
                 }
                 catch (Exception exp) {
                 }
             }
         });
    }

    Planner() throws ParseException
    {
    	int numberOfDays = 50;
    	String coursesString[]= 
    			{
    					"Course,SWEN424,High,Lecture,Monday,0810,1500",
    					"Course,SWEN424,High,Lecture,Tuesday,0810,1500",
    					"Course,SWEN424,High,Lecture,Wednesday,0810,1500",
    					"Course,COMP307,Medium,Lecture,Tuesday,1200,1300",
    					"Course,COMP307,Medium,Lecture,Wednesday,1200,1300",
    					"Course,COMP307,Medium,Tutorial,Friday,1200,1300",
    					"Course,SWEN432,Medium,Lecture,Monday,1100,1200",
    					"Course,SWEN432,Medium,Lecture,Wednesday,1100,1200",
    					"Course,SWEN432,Medium,Lecture,Friday,1100,1200",
    					"Meal,Breakfast,NA,NA,Monday,0800,0900",
    					"Meal,Breakfast,NA,NA,Tuesday,0800,0900",
    					"Meal,Breakfast,NA,NA,Wednesday,0800,0900",
    					"Meal,Breakfast,NA,NA,Thursday,0800,0900",
    					"Meal,Breakfast,NA,NA,Friday,0800,0900",
    					"Meal,Breakfast,NA,NA,Saturday,0800,0900",
    					"Meal,Breakfast,NA,NA,Sunday,0800,0900",
    					"Meal,Lunch,NA,NA,Monday,1300,1400",
    					"Meal,Lunch,NA,NA,Tuesday,1300,1400",
    					"Meal,Lunch,NA,NA,Wednesday,1300,1400",
    					"Meal,Lunch,NA,NA,Thursday,1300,1400",
    					"Meal,Lunch,NA,NA,Friday,1300,1400",
    					"Meal,Lunch,NA,NA,Saturday,1300,1400",
    					"Meal,Lunch,NA,NA,Sunday,1300,1400",
    					"Meal,Dinner,NA,NA,Monday,1800,1900",
    					"Meal,Dinner,NA,NA,Tuesday,1800,1900",
    					"Meal,Dinner,NA,NA,Wednesday,1800,1900",
    					"Meal,Dinner,NA,NA,Thursday,1800,1900",
    					"Meal,Dinner,NA,NA,Friday,1800,1900",
    					"Meal,Dinner,NA,NA,Saturday,1800,1900",
    					"Meal,Dinner,NA,NA,Sunday,1800,1900",
    					"Rest,Sleeping,NA,NA,Monday,2200,0600",
    					"Rest,Sleeping,NA,NA,Tuesday,2200,0600",
    					"Rest,Sleeping,NA,NA,Wednesday,2200,0600",
    					"Rest,Sleeping,NA,NA,Thursday,2200,0600",
    					"Rest,Sleeping,NA,NA,Friday,2200,0600",
    					"Rest,Sleeping,NA,NA,Saturday,2200,0600",
    					"Rest,Sleeping,NA,NA,Sunday,2200,0600"
				};	
    	
    	setLayout(new BorderLayout());
    	TaskCreatePanel taskPanel = new TaskCreatePanel();	
    	//add(taskPanel, BorderLayout.EAST);
    	//add(calendar, BorderLayout.CENTER);
    	
    	getContentPane().add(calendar, BorderLayout.CENTER);
    	getContentPane().add(taskPanel, BorderLayout.WEST);
    	
    	calendar.setCurrentView(CalendarView.Timetable);
		calendar.setTheme(ThemeType.Light);
		DateTime today = DateTime.today();

		calendar.getTimetableSettings().getDates().clear();
		for (int i = 0; i < numberOfDays; i++)
			calendar.getTimetableSettings().getDates().add(today.addDays(i));

		// Set the visible columns to 3
		calendar.getTimetableSettings().setVisibleColumns(7);
		
		
		for(int j=0;j<coursesString.length;j++)
		{
			System.out.println(coursesString[j]);
			String splitStr[] = coursesString[j].split(",");
			
			int num1 = Integer.parseInt(splitStr[5]);
			int num2 = Integer.parseInt(splitStr[6]);
			

			
			if(num1>num2)
			{
				String nightTime = splitStr[5];
				String mrngTime = splitStr[6];
				splitStr[6] = "2400";
				Appointment app = appointmentMake(splitStr);
				calendar.getSchedule().getItems().add(app);
				
				splitStr[5] = "0000";
				splitStr[6] = mrngTime;
				
				switch(splitStr[4]) {
		         case "Monday" :
		        	splitStr[4] = "Tuesday"; 
		            break;
		         case "Tuesday" :
		        	 splitStr[4] = "Wednesday"; 
			         break;
		         case "Wednesday" :
		        	 splitStr[4] = "Thursday"; 
			         break;
		         case "Thursday" :
		        	 splitStr[4] = "Friday"; 
			            break;
		         case "Friday" :
		        	 splitStr[4] = "Saturday"; 
			            break;
		         case "Saturday" :
		        	 splitStr[4] = "Sunday"; 
			            break;
		         case "Sunday" :
		        	 splitStr[4] = "Monday"; 
			            break;
		         default :
		        	 break;
		        	 
		      }
				
				Appointment app1 = appointmentMake(splitStr);
				calendar.getSchedule().getItems().add(app1);
			}
			else
			{
				Appointment app = appointmentMake(splitStr);
				calendar.getSchedule().getItems().add(app);
			}
			
		}		
		
		
    	calendar.endInit();
    	
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(400, 400);
        setTitle("Planner");
       // calendar.getDate().
    }
    
    public static void scheduleApp(String details) throws ParseException
    {
    	System.out.println("action perfomed");
    	String s= "SWEN424,24/05/2019,Assignment";
    	//Project,Assignment1,25,24/05/2019,SWEN432
    	String spl[] = details.split(",");
    	int spendHours = Integer.parseInt(spl[2]);
    	String[] arr = s.split(",");
    //	Appointment app = appointmentMake(arr);
    //	calendar.getSchedule().getItems().add(app);
    	//System.out.println("arr[1] " + arr[1]);
    	String dateStr = spl[3].replaceAll("/", "");
    	//System.out.println("datestr " + dateStr);
    	DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		Date dueDate = dateFormat.parse(dateStr);
		//System.out.println("dueDate: " + dueDate);
		DateTime ddt = new DateTime(dueDate);
		//System.out.println("ddt: " + ddt);
		
		//calendar.ca
		System.out.println(DateTime.op_Subtraction(ddt, DateTime.now()).getDays());
		int remainingDays = DateTime.op_Subtraction(ddt, DateTime.now()).getDays();
		System.out.println(remainingDays);
		System.out.println(spendHours);
		
		double perdayWork = spendHours/remainingDays;
		System.out.println(perdayWork);
		//int difference = ddt - DateTime.now();
		List<Integer> superlist = computeRepartitionNumber(spendHours, remainingDays);
		System.out.println("Schedule:");
		Schedule sch = calendar.getSchedule();
		double dur=2;
		Duration dura = Duration.fromHours(2);
		
	/*	String tomo="19052019";
		Date tomoDate = dateFormat.parse(tomo);
		DateTime todt = new DateTime(tomoDate);*/
		//System.out.println(sch.getFreePeriod(todt, ddt, dura));
		DateTime dateVal = DateTime.today();
		
		System.out.println("values");
		for(int i=0;i<remainingDays;i++)
		{
			Duration duran = Duration.fromHours(superlist.get(i));
			dateVal = dateVal.addHours(24);
			//System.out.println(dateVal);
			if(superlist.get(i)!=0)
			{
				DateTime newDateVal = dateVal.addHours(24);
				System.out.println(dateVal + "    " + newDateVal);
				System.out.println(sch.getFreePeriod(dateVal, newDateVal, duran));
				DateTime start = sch.getFreePeriod(dateVal, newDateVal, duran);
				DateTime end = start.addHours(superlist.get(i));
				//"Course,SWEN424,High,Lecture,Tuesday,1410,1900"
				
				Appointment app = new Appointment();
				app.setHeaderText(spl[0] + ":" + spl[4]);
				app.setDescriptionText(spl[1]);
				app.setStartTime(start);
				app.setEndTime(end);
				Style style = app.getStyle();
				style.setBorderTopColor(Colors.Gold);
				style.setBorderLeftColor(Colors.Gold);
				style.setBorderBottomColor(Colors.Gold);
				style.setBorderRightColor(Colors.Gold);
				style.setLineColor(Colors.Gold);
				style.setFillColor(Colors.DarkRed);
				style.setBrush(new GradientBrush(Colors.White, Colors.LightGreen, 90));
				style.setHeaderTextColor(Colors.DarkRed);
				style.setTextColor(Colors.DarkRed);
				calendar.getSchedule().getItems().add(app);
				
			}
			else
			{
				
			}
		}
		
		
    }
    
    public static List<Integer> computeRepartitionNumber(int spendHours, int remainingDays) {
        
        int perdayWork=0;
        ArrayList<Integer> unitary = new ArrayList<>();
        int sum=0;
        if(!(spendHours%remainingDays == 0))
        {
        	perdayWork = spendHours/remainingDays;
            System.out.println("perday work: "+perdayWork);
            for(int i=0;i<remainingDays;i++)
            {
            	
            	if((sum + perdayWork+1)>spendHours)
            	{
            		unitary.add(spendHours-sum);
            		sum = sum + (spendHours-sum);
            	}
            	else
            	{
            		unitary.add(perdayWork+1);
            		sum = sum + perdayWork+1;
            	}
            }
            
            System.out.println("sum: "+sum);
        }
        else
        {
        	perdayWork = spendHours/remainingDays;
        	for(int i=0;i<remainingDays;i++)
        	{
        		unitary.add(perdayWork);
        		sum = sum + perdayWork;
        	}
        }
        System.out.println("printlist: " + sum);
        for(Integer a:unitary)
        {
        	 System.out.print(a + " ");
        }
        
        return unitary;

    }
    
    public static Appointment appointmentMake(String splitStr[]) throws ParseException
    {
    	//System.out.println(splitStr[5]);
    	//System.out.println(splitStr[6]);
    	Appointment app = new Appointment();
		app.setHeaderText(splitStr[1]);
		
		if(!splitStr[2].equals("NA"))
		{
			String desc = "Difficulty: " + splitStr[2] + "\n" + splitStr[3];
			app.setDescriptionText(desc);
		}
		

		DateFormat dateFormat = new SimpleDateFormat("hhmm");
		//Parse the string of time to time object.
		/*
		 * if(splitStr[2].startsWith("12") {
		 * 
		 * }
		 */

		Date startTim = dateFormat.parse(splitStr[5]);
		Date endTim = dateFormat.parse(splitStr[6]);
		DateTime sdt = new DateTime(startTim);
		DateTime edt = new DateTime(endTim);
		
		if(splitStr[5].startsWith("12"))
		{
			System.out.println("splitStr[5]" + splitStr[5]);
			StringBuilder spt5 = new StringBuilder(splitStr[5]);
			spt5.setCharAt(1, '1');
			System.out.println("splitStr[5] changes: " + spt5.toString());
			startTim = dateFormat.parse(spt5.toString());
			sdt = new DateTime(startTim);
			System.out.println("sdt changes1: " + sdt.toString());
			sdt = sdt.addHours(1);
			System.out.println("sdt changes: " + sdt.toString());
		}
		if(splitStr[6].startsWith("12"))
		{
			System.out.println("splitStr[6]" + splitStr[6]);
			StringBuilder spt6 = new StringBuilder(splitStr[6]);
			spt6.setCharAt(1, '1');	
			endTim = dateFormat.parse(spt6.toString());
			edt = new DateTime(endTim);
			edt = edt.addHours(1);
		}
		
		
		
		System.out.println(startTim.toString());
		System.out.println(endTim.toString());
		System.out.println(sdt.toString());
		System.out.println(edt.toString());
		

		Recurrence rec = new Recurrence();
		app.setRecurrence(rec);
		
		EnumSet<DayOfWeek> setEn = null;
	      switch(splitStr[4]) {
	         case "Monday" :
	        	//System.out.println("Monday");
	 			setEn = EnumSet.of(DayOfWeek.Monday); 
	            break;
	         case "Tuesday" :
	        	//System.out.println("Tuesday");
		 		setEn = EnumSet.of(DayOfWeek.Tuesday);
		        break;
	         case "Wednesday" :
	        	//System.out.println("Wednesday");
		 		setEn = EnumSet.of(DayOfWeek.Wednesday);
		        break;
	         case "Thursday" :
	        	// System.out.println("Thursday");
		 		setEn = EnumSet.of(DayOfWeek.Thursday);
		        break;
	         case "Friday" :
	        	 //System.out.println("Friday");
		 		setEn = EnumSet.of(DayOfWeek.Friday); 
		        break;
	         case "Saturday" :
	        	 System.out.println("Saturday");
		 		setEn = EnumSet.of(DayOfWeek.Saturday);
		        break;
	         case "Sunday" :
	        	 //System.out.println("Sunday");
		 		setEn = EnumSet.of(DayOfWeek.Sunday);
		        break;
	         default :
	        	 break;
	        	 
	      }
	   //  System.out.println(setEn.toString());
	    rec.setDaysOfWeek(setEn);
		app.setStartTime(sdt);
		app.setEndTime(edt);
		app.setRecurrence(rec);
		
		if(splitStr[0].equals("Course"))
		{
			Style style = app.getStyle();
			style.setBorderTopColor(Colors.DarkOrchid);
			style.setBorderLeftColor(Colors.DarkOrchid);
			style.setBorderBottomColor(Colors.DarkOrchid);
			style.setBorderRightColor(Colors.DarkOrchid);
			style.setLineColor(Colors.DarkOrchid);
			style.setFillColor(Colors.MediumOrchid);
			style.setBrush(new GradientBrush(Colors.White, Colors.Plum, 90));
			style.setHeaderTextColor(Colors.DarkViolet);
			style.setTextColor(Colors.DarkViolet);
		}
		else if(splitStr[0].equals("Meal"))
		{
			Style style = app.getStyle();
			style.setBorderTopColor(Colors.Firebrick);
			style.setBorderLeftColor(Colors.Firebrick);
			style.setBorderBottomColor(Colors.Firebrick);
			style.setBorderRightColor(Colors.Firebrick);
			style.setLineColor(Colors.Firebrick);
			style.setFillColor(Colors.Brown);
			style.setBrush(new GradientBrush(Colors.White, Colors.BurlyWood, 90));
			style.setHeaderTextColor(Colors.Firebrick);
			style.setTextColor(Colors.Firebrick);
		}
		else if(splitStr[0].equals("Rest"))
		{
			Style style = app.getStyle();
			style.setBorderTopColor(Colors.DarkGreen);
			style.setBorderLeftColor(Colors.DarkGreen);
			style.setBorderBottomColor(Colors.DarkGreen);
			style.setBorderRightColor(Colors.DarkGreen);
			style.setLineColor(Colors.DarkGreen);
			style.setFillColor(Colors.DarkOliveGreen);
			style.setBrush(new GradientBrush(Colors.White, Colors.LightYellow, 90));
			style.setHeaderTextColor(Colors.DarkGreen);
			style.setTextColor(Colors.DarkGreen);
		}
		
		return app;
    }

}