As MPS store absolute path of the external jar, to run the application, need to remove the jar and re-add
back to the solution again:

0. Unzip "projectmanagement.zip"
1. Right click on "PlanningLanguage.sandbox" solution
2. Select "Module properties"
3. In common tab, if "java_classes..." is flagged as error, please removed it
4. Also in Java tab, remove "..../JPlanner.jar"
5. Go to "Commmon" tab and click 'Add Model Root', select "Java Classes"
6. Browse to "JPlanner.jar" (which should be at root folder under "projectmanagement")
7. Once added, on the right panel, select the root folder and click blue "Sources" icon on top to add it
8. In Java tab, click "+" from "Libraries" panel and browse to "JPlanner.jar"
9. In "Model Roots" window, select "java_source_stubs" from the dropdownlist and click "OK"
10. Click "Apply" and "OK"
11. You should see additional "Stubs" folder appears below within the solution below sandbox.
