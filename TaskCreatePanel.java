import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.ParseException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;

import com.mindfusion.scheduling.model.Appointment;

public class TaskCreatePanel extends JPanel {

	private JLabel categoryLabel;
	private JTextField categoryField;
	private JComboBox comboForType;

	private JLabel nameLabel;
	private JTextField nameField;
	private JComboBox comboFordifficulty;

	private JLabel hoursLabel;
	private JTextField hoursField;

	private JLabel dueLabel;
	private JTextField dueField;

	private JLabel courseLabel;
	private JTextField courseField;

	private JButton scheduleButton;

	public TaskCreatePanel(){

		String[] TypeStrings = { "Project", "Assignment"};
		comboForType = new JComboBox(TypeStrings);

		nameLabel = new JLabel("Project/Assignment Name:");
		nameField = new JTextField(20);

		scheduleButton = new JButton("Schedule");
		scheduleButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
					System.out.println("action perfomed");
					String categoryV =  comboForType.getSelectedItem().toString();
					String difficultyV = nameField.getText();
					String hoursV = hoursField.getText();
					String dueDateV = dueField.getText();
					String courseV = courseField.getText();

					String details = categoryV + "," + difficultyV + "," + hoursV + "," + dueDateV + "," + courseV;
					System.out.println("detailes: " + details);
					try {
						Planner.scheduleApp(details);
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}
		});

	categoryLabel = new JLabel("Category:");
	categoryField = new JTextField(20);

/*	nameLabel = new JLabel("Difficulty:");
	nameField = new JTextField(20);
	*/
	hoursLabel = new JLabel("Estimated hours:");
	hoursField = new JTextField(20);

	dueLabel = new JLabel("Due date:");
	dueField = new JTextField(20);

	courseLabel = new JLabel("Course Name:");
	courseField = new JTextField(20);
	setComponents();
	}

	private void setComponents(){
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		Insets basic = new Insets(5,2,5,5);
		gbc.insets = basic;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.BOTH;

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridx = 1;
		//add(dayList, gbc);
		gbc.gridy = 1;
		//add(colorList, gbc);
		gbc.gridy = 2;
		add(comboForType, gbc);
		gbc.gridy = 3;
		add(nameField, gbc);
		gbc.gridy = 4;
		add(hoursField, gbc);
		gbc.gridy = 5;
		add(dueField, gbc);
		gbc.gridy = 6;
		add(courseField, gbc);
		gbc.gridy = 7;
		add(scheduleButton, gbc);
		gbc.gridx = 0;
		gbc.gridy = 6;
		add(courseLabel, gbc);
		gbc.gridy = 5;
		add(dueLabel, gbc);
		gbc.gridy = 4;
		add(hoursLabel, gbc);
		gbc.gridy = 3;
		add(nameLabel, gbc);
		gbc.gridy = 2;
		add(categoryLabel, gbc);

	}

}
