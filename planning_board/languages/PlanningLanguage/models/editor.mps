<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:94b3ff92-e4d3-4f5c-b607-121ff3f30c61(PlanningLanguage.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="12" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="y8l8" ref="r:80473c4f-1004-470e-b186-9d0135c160bc(PlanningLanguage.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="5oXTtSimSRE">
    <ref role="1XX52x" to="y8l8:5oXTtSimSQJ" resolve="Column" />
    <node concept="3EZMnI" id="5oXTtSimSRG" role="2wV5jI">
      <node concept="3F0ifn" id="5oXTtSimSRN" role="3EZMnx">
        <property role="3F0ifm" value="Column Name: " />
      </node>
      <node concept="3F0A7n" id="5oXTtSimSRX" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="l2Vlx" id="5oXTtSimSRJ" role="2iSdaV" />
      <node concept="3F0ifn" id="5oXTtSimTWl" role="3EZMnx">
        <property role="3F0ifm" value="Description: " />
        <node concept="pVoyu" id="5oXTtSimTYf" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="5oXTtSimTW_" role="3EZMnx">
        <ref role="1NtTu8" to="y8l8:5oXTtSimTWf" resolve="description" />
      </node>
      <node concept="3F0ifn" id="5oXTtSin6Wk" role="3EZMnx">
        <property role="3F0ifm" value="Tasks:" />
        <node concept="pVoyu" id="5oXTtSin6WX" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="5oXTtSin6Xh" role="3EZMnx">
        <node concept="pVoyu" id="5oXTtSin6Yh" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="5oXTtSin6XQ" role="3EZMnx">
        <ref role="1NtTu8" to="y8l8:5oXTtSimSRf" resolve="tasks" />
        <node concept="2iRkQZ" id="5oXTtSin6XT" role="2czzBx" />
        <node concept="VPM3Z" id="5oXTtSin6XU" role="3F10Kt" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="5oXTtSimTZu">
    <ref role="1XX52x" to="y8l8:5oXTtSimSQG" resolve="Board" />
    <node concept="3EZMnI" id="5oXTtSimTZw" role="2wV5jI">
      <node concept="3F0ifn" id="5oXTtSimTZB" role="3EZMnx">
        <property role="3F0ifm" value="Board Name:" />
      </node>
      <node concept="3F0A7n" id="5oXTtSimTZL" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="5oXTtSimTZT" role="3EZMnx">
        <property role="3F0ifm" value="Description: " />
        <node concept="pVoyu" id="5oXTtSimU0f" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="5oXTtSimU09" role="3EZMnx">
        <ref role="1NtTu8" to="y8l8:5oXTtSimTZ3" resolve="description" />
      </node>
      <node concept="3F0ifn" id="5oXTtSin3FJ" role="3EZMnx">
        <property role="3F0ifm" value="Columns:" />
        <node concept="pVoyu" id="5oXTtSin3Hl" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="5oXTtSin3HJ" role="3EZMnx">
        <node concept="pVoyu" id="5oXTtSin3I7" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="5oXTtSin3Gc" role="3EZMnx">
        <ref role="1NtTu8" to="y8l8:5oXTtSimSQQ" resolve="column" />
        <node concept="2iRkQZ" id="5oXTtSin3Gf" role="2czzBx" />
        <node concept="VPM3Z" id="5oXTtSin3Gg" role="3F10Kt" />
      </node>
      <node concept="l2Vlx" id="5oXTtSimTZz" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5oXTtSimU0E">
    <ref role="1XX52x" to="y8l8:5oXTtSimSQS" resolve="Task" />
    <node concept="3EZMnI" id="5oXTtSimU0G" role="2wV5jI">
      <node concept="3F0ifn" id="5oXTtSimU0N" role="3EZMnx">
        <property role="3F0ifm" value="Task name:" />
      </node>
      <node concept="3F0A7n" id="5oXTtSimU0X" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="5oXTtSimU15" role="3EZMnx">
        <property role="3F0ifm" value="Description: " />
        <node concept="pVoyu" id="5oXTtSimU2H" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="5oXTtSimU1l" role="3EZMnx">
        <ref role="1NtTu8" to="y8l8:5oXTtSimSQX" resolve="description" />
      </node>
      <node concept="3F0ifn" id="5oXTtSimU1x" role="3EZMnx">
        <property role="3F0ifm" value="Start Date: " />
        <node concept="pVoyu" id="5oXTtSimU2J" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="5oXTtSimU1R" role="3EZMnx">
        <ref role="1NtTu8" to="y8l8:5oXTtSimSR0" resolve="start_date" />
      </node>
      <node concept="3F0ifn" id="5oXTtSimU27" role="3EZMnx">
        <property role="3F0ifm" value="End Date" />
      </node>
      <node concept="3F0A7n" id="5oXTtSimU2z" role="3EZMnx">
        <ref role="1NtTu8" to="y8l8:5oXTtSimSR6" resolve="end_date" />
      </node>
      <node concept="3F0ifn" id="5oXTtSindCe" role="3EZMnx">
        <node concept="pVoyu" id="5oXTtSindCA" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="5oXTtSimU0J" role="2iSdaV" />
    </node>
  </node>
</model>

