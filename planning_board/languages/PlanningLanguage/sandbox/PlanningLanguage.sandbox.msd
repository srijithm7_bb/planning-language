<?xml version="1.0" encoding="UTF-8"?>
<solution name="PlanningLanguage.sandbox" uuid="4f907f09-737b-4a2f-a84b-6700e66bdb80" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions>
    <language slang="l:c4707845-a7aa-413c-8087-42bb52474f65:PlanningLanguage" version="0" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
  </languageVersions>
  <dependencyVersions>
    <module reference="4f907f09-737b-4a2f-a84b-6700e66bdb80(PlanningLanguage.sandbox)" version="0" />
  </dependencyVersions>
</solution>

