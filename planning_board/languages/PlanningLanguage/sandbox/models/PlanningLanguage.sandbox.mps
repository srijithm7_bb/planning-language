<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:773f8590-1c05-45e0-ba92-1fe8496a5b2c(PlanningLanguage.sandbox)">
  <persistence version="9" />
  <languages>
    <use id="c4707845-a7aa-413c-8087-42bb52474f65" name="PlanningLanguage" version="0" />
  </languages>
  <imports />
  <registry>
    <language id="c4707845-a7aa-413c-8087-42bb52474f65" name="PlanningLanguage">
      <concept id="6214375802794642872" name="PlanningLanguage.structure.Task" flags="ng" index="14pAxA">
        <property id="6214375802794642886" name="end_date" index="14pAwo" />
        <property id="6214375802794642880" name="start_date" index="14pAwu" />
        <property id="6214375802794642877" name="description" index="14pAxz" />
      </concept>
      <concept id="6214375802794642863" name="PlanningLanguage.structure.Column" flags="ng" index="14pAxL">
        <property id="6214375802794647311" name="description" index="14pBFh" />
        <child id="6214375802794642895" name="tasks" index="14pAwh" />
      </concept>
      <concept id="6214375802794642860" name="PlanningLanguage.structure.Board" flags="ng" index="14pAxM">
        <property id="6214375802794647491" name="description" index="14pBCt" />
        <child id="6214375802794642870" name="column" index="14pAxC" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="14pAxM" id="5oXTtSimXa$">
    <property role="TrG5h" value="University Work" />
    <property role="14pBCt" value="Helps me organise my university work" />
    <node concept="14pAxL" id="5oXTtSingWy" role="14pAxC">
      <property role="TrG5h" value="SWEN421" />
      <property role="14pBFh" value="A column for SWEN421" />
      <node concept="14pAxA" id="5oXTtSingWL" role="14pAwh">
        <property role="TrG5h" value="Project Report" />
        <property role="14pAxz" value="The group report for this project" />
        <property role="14pAwu" value="10th of May" />
        <property role="14pAwo" value="17th of May" />
      </node>
      <node concept="14pAxA" id="5oXTtSingWO" role="14pAwh">
        <property role="TrG5h" value="Project Language" />
        <property role="14pAxz" value="The project language for this project" />
        <property role="14pAwu" value="3rd of May" />
        <property role="14pAwo" value="17th of May" />
      </node>
      <node concept="14pAxA" id="5oXTtSingWT" role="14pAwh">
        <property role="TrG5h" value="Exam" />
        <property role="14pAxz" value="Exam for this course" />
        <property role="14pAwu" value="17th of June" />
        <property role="14pAwo" value="17th of June" />
      </node>
    </node>
    <node concept="14pAxL" id="5oXTtSingWG" role="14pAxC">
      <node concept="14pAxA" id="5oXTtSingXd" role="14pAwh" />
    </node>
  </node>
</model>

